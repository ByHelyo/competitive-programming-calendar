import { writable } from 'svelte/store';

export const contests = writable([]);
export const challenges = writable(new Map());
export const event = writable('Upcoming');
