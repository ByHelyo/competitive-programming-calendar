export function compareDatesEvent(date_a, date_b, event) {
	if (event === 'Upcoming') {
		if (date_a.end.getTime() > date_b) {
			return true;
		}

		return false;
	}

	if (event === 'Past') {
		if (date_a.start.getTime() < date_b) {
			return true;
		}

		return false;
	}

	return date_a.start.getTime() >= date_b && date_a.end.getTime() <= date_b.end;
}
