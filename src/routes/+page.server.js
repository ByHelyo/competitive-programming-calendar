export async function load() {
	const base_url =
		'https://competitiveprogrammingcalendar.agreeabletree-1f3a62a8.eastus.azurecontainerapps.io';

	let contests = await fetchGet(base_url + '/api/contests');
	let challenges = await fetchGet(base_url + '/api/challenges');

	challenges = new Map(
		challenges
			.sort((a, b) => {
				return a.name.localeCompare(b.name);
			})
			.map((challenge) => {
				return [challenge.name, true];
			})
	);

	contests = contests
		.map((contest) => {
			return {
				...contest,
				start: new Date(contest.start),
				end: new Date(contest.end)
			};
		})
		.sort((a, b) => {
			let start_earlier = a.start.getTime() - b.start.getTime();
			if (start_earlier !== 0) {
				return start_earlier;
			}

			let end_earlier = a.end.getTime() - b.end.getTime();
			if (end_earlier !== 0) {
				return end_earlier;
			}

			return a.name.localeCompare(b.name);
		});

	return {
		challenges: challenges,
		contests: contests
	};
}

async function fetchGet(url) {
	const res = await fetch(url, {});

	return await res.json();
}
